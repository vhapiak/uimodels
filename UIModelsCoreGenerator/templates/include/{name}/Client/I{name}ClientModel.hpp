{%- if context.model -%}

#pragma once

#include <memory>

{% for include in utils.generateModelNativeIncludes() -%}
    #include <{{ include }}>
{% endfor %}

#include "UIModels/IUIModel.hpp"
#include "UIModels/Client/TEvent.hpp"

{% for scope in utils.generateModelScopesDependencies() -%}
    #include "{{ scope.name }}/{{ scope.name }}Types.hpp"
{%- endfor %}

{% for scope in utils.generatePathFromRoot(context.scope) -%}
namespace {{ scope.name }} {
{% endfor -%}
namespace Client {

class I{{context.name}}ClientModel
    : virtual public ::MXR::UIModels::IUIModel
{
public: // methods
    virtual ~I{{context.name}}ClientModel() = default;

    {% for property in context.model.properties -%}
    virtual void set{{property.name}}({{utils.generateTypename(property.type)}} const& value) noexcept = 0; 
    {% endfor %}

    {% for command in context.model.commands -%}
    virtual void call{{command.name}}({{utils.generateArgumentsList(command)}}) noexcept = 0; 
    {% endfor %}

    {% for event in context.model.events -%}
    virtual ::MXR::UIModels::Client::TEvent<{{utils.generateTypename(event.type)}}> const& 
        get{{event.name}}Event() const noexcept = 0; 
    {% endfor %}
};

using {{context.name}}ClientModelUPtr = std::unique_ptr<I{{context.name}}ClientModel>;
using {{context.name}}ClientModelPtr = std::shared_ptr<I{{context.name}}ClientModel>;

}
{% for scope in utils.generatePathFromRoot(context.scope) -%}
}
{% endfor -%}

{%- endif -%}