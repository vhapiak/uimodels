{%- if context.model -%}

#pragma once

#include "UIModels/IUIModel.hpp"

#include "{{context.name}}/Client/I{{context.name}}ClientModel.hpp"

{% for scope in utils.generatePathFromRoot(context.scope) -%}
namespace {{ scope.name }} {
{% endfor -%}
namespace Client {
  
{{context.model.name}}ClientModelPtr createInstance(::MXR::UIModels::UIModelPtr const& ptr);

}
{% for scope in utils.generatePathFromRoot(context.scope) -%}
}
{% endfor -%}

{%- endif -%}