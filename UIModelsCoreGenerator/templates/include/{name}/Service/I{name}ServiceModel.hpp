{%- if context.model -%}

#pragma once

#include <memory>

{% for include in utils.generateModelNativeIncludes() -%}
    #include <{{ include }}>
{% endfor %}

#include "UIModels/IUIModel.hpp"
#include "UIModels/Service/TProperty.hpp"
#include "UIModels/Service/TCommand.hpp"

{% for scope in utils.generateModelScopesDependencies() -%}
    #include "{{ scope.name }}/{{ scope.name }}Types.hpp"
{%- endfor %}

{% for scope in utils.generatePathFromRoot(context.scope) -%}
namespace {{ scope.name }} {
{% endfor -%}
namespace Service {

class I{{context.name}}ServiceModel
    : virtual public ::MXR::UIModels::IUIModel
{
public: // types
    {% for command in context.model.commands -%}
    using {{command.name}}Command = ::MXR::UIModels::Service::TCommand<{{utils.generateTypesList(command)}}>; 
    {% endfor %}

public: // methods
    virtual ~I{{context.name}}ServiceModel() = default;

    {% for property in context.model.properties -%}
    virtual ::MXR::UIModels::Service::TProperty<{{utils.generateTypename(property.type)}}> const& 
        get{{property.name}}Property() const noexcept = 0; 
    {% endfor %}

    {% for command in context.model.commands -%}
    virtual {{command.name}}Command const& get{{command.name}}Command() noexcept = 0; 
    {% endfor %}

    {% for event in context.model.events -%}
    virtual void trigger{{event.name}}({{utils.generateTypename(event.type)}} const& value) noexcept = 0; 
    {% endfor %}
};

using {{context.name}}ServiceModelUPtr = std::unique_ptr<I{{context.name}}ServiceModel>;
using {{context.name}}ServiceModelPtr = std::shared_ptr<I{{context.name}}ServiceModel>;

}
{% for scope in utils.generatePathFromRoot(context.scope) -%}
}
{% endfor -%}

{%- endif -%}