{%- if context.model -%}

#pragma once

#include "{{context.name}}/Service/I{{context.name}}ServiceModel.hpp"

{% for scope in utils.generatePathFromRoot(context.scope) -%}
namespace {{ scope.name }} {
{% endfor -%}
namespace Service {
  
{{context.model.name}}ServiceModelUPtr createInstance();

}
{% for scope in utils.generatePathFromRoot(context.scope) -%}
}
{% endfor -%}

{%- endif -%}