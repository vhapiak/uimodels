
#pragma once

{% for include in utils.generateTypesNativeIncludes() -%}
    #include <{{ include }}>
{% endfor %}

{% for scope in utils.generateTypesScopesDependencies() %}
    {%- if scope != context.scope -%}
        #include "{{ scope.name }}/{{ scope.name }}Types.hpp"
    {%- endif -%}
{%- endfor %}

{% for scope in utils.generatePathFromRoot(context.scope) -%}
namespace {{ scope.name }} {
{% endfor -%}

{% for type in context.types -%}
    {% if type.values %}
        enum class {{ type.name }}
        {
            {% for value in type.values -%}
                {{value}},
            {% endfor %}
        };
    {% endif %}

    {%- if type.fields %}
        struct {{ type.name }}
        {
            {% for field in type.fields -%}
                {{utils.generateTypename(field.type)}} {{field.name}};
            {% endfor %}
        };
    {% endif %}
{%- endfor %}

{% for scope in utils.generatePathFromRoot(context.scope) -%}
}
{% endfor -%}