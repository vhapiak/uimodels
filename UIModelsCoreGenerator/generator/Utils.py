
class Utils:
    def __init__(self, context):
        self.context = context

    def generatePathFromRoot(self, scope):
        path = []
        while scope.parent is not None:
            path.insert(0, scope)
            scope = scope.parent
        return path

    def generateTypesNativeIncludes(self):
        includes = set()
        for _type in self.context.types:
            if hasattr(_type, 'fields'):
                for field in _type.fields:
                    if hasattr(field.type, 'include'):
                        includes.add(field.type.include)
        return list(includes)

    def generateTypesScopesDependencies(self):
        dependencies = set()
        for _type in self.context.types:
            if hasattr(_type, 'fields'):
                for field in _type.fields:
                    if not hasattr(field.type, 'include'):
                        dependencies.add(field.type.scope)
        return list(dependencies)

    def generateModelNativeIncludes(self):
        includes = set()        
        if self.context.model is None:
            return list(dependencies)

        for _property in self.context.model.properties:
            if hasattr(_property.type, 'include'):
                includes.add(_property.type.include)

        for command in self.context.model.commands:
            for arg in command.arguments:
                if hasattr(arg.type, 'include'):
                    includes.add(arg.type.include)

        return list(includes)

    def generateModelScopesDependencies(self):
        dependencies = set()
        if self.context.model is None:
            return list(dependencies)

        for _property in self.context.model.properties:
            if not hasattr(_property.type, 'include'):
                dependencies.add(_property.type.scope)

        for command in self.context.model.commands:
            for arg in command.arguments:
                if not hasattr(arg.type, 'include'):
                    dependencies.add(arg.type.scope)

        return list(dependencies)

    def generateTypename(self, type):
        if self.context.scope == type.scope or self.context.globalScope == type.scope:
            return type.name
        name = '::' + type.name
        scope = type.scope
        while scope.parent:
            name = '::' + scope.name + name
            scope = scope.parent
        return name

    def generateModelType(self, model):
        name = '::' + model.name
        scope = model.scope
        while scope.parent:
            name = '::' + scope.name + name
            scope = scope.parent
        return name

    def generateTypesList(self, command):
        result = ''
        if len(command.arguments) > 0:
            for arg in command.arguments[:-1]:
                result += self.generateTypename(arg.type) + ', '
            result += self.generateTypename(command.arguments[-1].type)
        return result

    def generateArgumentsList(self, command):
        result = ''
        if len(command.arguments) > 0:
            for arg in command.arguments[:-1]:
                result += self.generateTypename(arg.type) + ' const& ' + arg.name + ', '
            result += self.generateTypename(command.arguments[-1].type) + ' const& ' + command.arguments[-1].name
        return result

    def generateVariablesList(self, command):
        result = ''
        if len(command.arguments) > 0:
            for arg in command.arguments[:-1]:
                result += arg.name + ', '
            result += command.arguments[-1].name
        return result