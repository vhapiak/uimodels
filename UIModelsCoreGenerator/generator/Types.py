class Type:
    def __init__(self, name, scope):
        self.name = name
        self.scope = scope
        scope.types[name] = self # register type in scope


class NativeType(Type):
    def __init__(self, name, scope, include):
        Type.__init__(self, name, scope)
        self.include = include


class CustomType(Type):
    def __init__(self, name, scope, fields):
        Type.__init__(self, name, scope)
        self.fields = fields


class EnumerationType(Type):
    def __init__(self, name, scope, values):
        Type.__init__(self, name, scope)
        self.values = values


class Scope:
    def __init__(self, name, parent):
        self.name = name
        self.parent = parent
        self.children = {}
        self.types = {}

        if parent is not None:
            parent.children[name] = self


class Field:
    def __init__(self, name, _type):
        self.name = name
        self.type = _type


class Property:
    def __init__(self, name, _type, defaultValue, cacheable):
        self.name = name
        self.type = _type
        self.defaultValue = defaultValue
        self.cacheable = cacheable


class Command:
    def __init__(self, name, arguments):
        self.name = name
        self.arguments = arguments


class Model:
    def __init__(self, name, scope, properties, commands, events):
        self.name = name
        self.scope = scope
        self.properties = properties
        self.commands = commands
        self.events = events
    
class Context:
    def __init__(self, name, globalScope, scope):
        self.name = name
        self.globalScope = globalScope
        self.scope = scope
        self.types = []
        self.model = None