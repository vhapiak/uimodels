import os
import argparse
import jinja2

from Generator import loadFile
from Utils import Utils

cmd = argparse.ArgumentParser(description='Generates UI models code')
cmd.add_argument('--model', required=True, help='Path to UI model yaml')
cmd.add_argument('--templates', required=True, help='Path to directory with templates')
cmd.add_argument('--out', required=True, help='Path to output directory')
cmd.add_argument('--importPath', default='', help='Semicolon separated pathes to imports')
args = cmd.parse_args()

importPath = args.importPath.split(':')
context = loadFile(importPath, args.model)
utils = Utils(context)

jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader(args.templates))

def renderFile(source, target):
    template = jinja_env.get_template(source)
    output = template.render(context=context, utils=utils)

    if output.strip():
        destpath = os.path.join(args.out, target)

        directory = os.path.dirname(destpath)
        if not os.path.isdir(directory):
            os.makedirs(directory)

        file = open(destpath, "w")
        file.write(output)
        file.close()

def renderDir(level, targetLevel):
    pathInTemplate = os.path.join(args.templates, level)
    files = os.listdir(pathInTemplate)
    for file in files:
        targetName = file.format(name=context.name)
        targetPath = os.path.join(targetLevel, targetName)

        nextLevel = os.path.join(level, file) 
        pathToFile = os.path.join(pathInTemplate, file)
        if os.path.isdir(pathToFile):
            renderDir(nextLevel, targetPath)
        else:
            renderFile(nextLevel, targetPath)

renderDir('.', '.')