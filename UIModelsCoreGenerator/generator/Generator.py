import os
import yaml

from Types import NativeType, CustomType, EnumerationType, Property, Field, Model, Command, Scope, Context

globalScope = Scope('root', None)
NativeType('bool', globalScope, 'cstdint')
NativeType('int32_t', globalScope, 'cstdint')
NativeType('uint32_t', globalScope, 'cstdint')
NativeType('double', globalScope, 'cstdint')

def getScope(root, path):
    if len(path) == 0:
        return root
    
    scope = root.children.get(path[0])
    if scope is None:
        scope = Scope(path[0], root)
    
    return getScope(scope, path[1:])


def resolveType(context, typename):
    scopePath = parseScope(typename)
    name = scopePath[-1]

    rootLookUp = getScope(globalScope, scopePath[:-1]) # todo prevent new scope creation
    localLookUp = context.scope

    _type = localLookUp.types.get(name)
    if _type is None:
        _type = rootLookUp.types.get(name)
    if _type is None:
        raise Exception('Cannot find type ' + typename)

    return _type


def parseScope(str):
    if str:
        return str.split('.') # todo use regex
    return []


def parseEnums(data, context):
    enums = data.get('types').get('enumerations')
    if enums is None:
        return

    for enumName in enums:
        values = enums.get(enumName)
        context.types.append(EnumerationType(enumName, context.scope, values))


def parseStructures(data, context):
    structures = data.get('types').get('structures')
    if structures is None:
        return
    for structureName in structures:
        structure = structures.get(structureName)
        fields = []
        for fieldName in structure:
            typename = structure.get(fieldName)
            _type = resolveType(context, typename)

            fields.append(Field(fieldName, _type))
        context.types.append(CustomType(structureName, context.scope, fields))


def parseNative(data, context):
    native = data.get('native')
    if native is None:
        return

    for typeName in native:
        _type = native.get(typeName)
        include = _type.get('include')
        if include is None:
            raise Exception('include field is required for native types ' + typeName)
        NativeType(typeName, context.scope, include)


def parseModelProperties(model, context):
    properties = model.get('properties')
    if properties is None:
        return []
    
    modelProperties = []
    for propertyName in properties:
        _property = properties.get(propertyName) 
        typename = _property.get('type')
        if typename is None:
            raise Exception('type field is required in property ' + propertyName)

        _type = resolveType(context, typename)
                
        defaultValue = _property.get('default')
        if defaultValue is None:
            defaultValue = '{}'

        cacheable = _property.get('cacheable')

        modelProperties.append(Property(propertyName, _type, defaultValue, cacheable))
    
    return modelProperties


def parseModelCommands(model, context):
    commands = model.get('commands')
    if commands is None:
        return []
    
    modelCommands = []
    for commandName in commands:
        command = commands.get(commandName) 
        arguments = command.get('arguments')
        if arguments is None:
            raise Exception('arguments field is required in command ' + commandName)
        
        commandArguments = []
        for argumentName in arguments:
            typename = arguments.get(argumentName)
            _type = resolveType(context, typename)
            commandArguments.append(Field(argumentName, _type))
                
        modelCommands.append(Command(commandName, commandArguments))
    
    return modelCommands


def parseModelEvents(model, context):
    events = model.get('events')
    if events is None:
        return []
    
    modelEvents = []
    for eventName in events:
        typename = events.get(eventName) 
        _type = resolveType(context, typename)
                
        modelEvents.append(Field(eventName, _type))
    
    return modelEvents


def parseModel(data, context):
    model = data.get('model')
    if model is None:
        return

    properties = parseModelProperties(model, context)
    commands = parseModelCommands(model, context)
    events = parseModelEvents(model, context)
    
    context.model = Model(context.name, context.scope, properties, commands, events)


def importType(importPath, data):
    imports = data.get('import')
    if imports is None:
        return

    for _import in imports: # todo add search in include path
        filename = _import + '.yml'
        found = False
        for path in importPath:
            fullpath = os.path.join(path, filename)
            if os.path.isfile(fullpath):
                found = True
                loadFile(importPath, fullpath)
        
        if not found:
            raise Exception('Cannot import ' + filename)


def loadFile(importPath, filepath):
    file = open(filepath)    
    data = yaml.load(file, Loader=yaml.Loader)
    file.close()
    
    if data.get('package') is None:
        raise Exception('package field is required in ' + filepath)

    name = data.get('name')
    if name is None:
        raise Exception('name field is required in ' + filepath)

    package = parseScope(data['package'])
    packageScope = getScope(globalScope, package)

    scope = getScope(packageScope, [name])

    context = Context(name, globalScope, scope)

    importType(importPath, data)

    if data.get('types') is not None:
        parseEnums(data, context)
        parseStructures(data, context)
    
    parseNative(data, context)
    parseModel(data, context)

    return context



