/////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2021 by Apostera GmbH                                     //
/////////////////////////////////////////////////////////////////////////////
//
// @file
// @author       Viktor Hapiak (viktor.hapiak@apostera.com)
// @project      MXR
// @sw_interface UIModelsNative/TCommand@1.0
// @ip_type      BGIP
// @brief
//

#ifndef HEADER_GUARD_VPWBKTBLUENHCDMF
#define HEADER_GUARD_VPWBKTBLUENHCDMF

#include "UIModels/Service/TCommand.hpp"
#include "UIModels/UIModelsTypes.hpp"

// IMPLEMENTATION_DETAILS_BEGINS
#include <map>
#include <tuple>
#include <vector>
// IMPLEMENTATION_DETAILS_ENDS

namespace MXR {
namespace UIModelsNative {

template <typename... Args>
class TCommand : public UIModels::Service::TCommand<Args...>
{
public: // types
    using Listener = std::function<void(Args const&...)>;

public: // method
    void call(Listener listener) const override;

    void deferedCall(Args const&... args);

    void fireUpdates(UIModels::Timestamp timestamp);

    void applyUpdates(UIModels::Timestamp timestamp);

    // IMPLEMENTATION_DETAILS_BEGINS
private: // types
    using Arguments = std::tuple<Args...>;
    using List = std::vector<Arguments>;
    using Map = std::map<UIModels::Timestamp, List>;

private: // fields
    List m_current_calls;

    List m_upcoming_calls;
    Map m_updates;
    // IMPLEMENTATION_DETAILS_ENDS
};

} // namespace UIModelsNative
} // namespace MXR

#include "UIModelsNative/Private/TCommand.inl"

#endif // HEADER_GUARD_VPWBKTBLUENHCDMF
