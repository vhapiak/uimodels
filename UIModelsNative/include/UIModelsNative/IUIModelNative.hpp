/////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2021 by Apostera GmbH                                     //
/////////////////////////////////////////////////////////////////////////////
//
// @file
// @author       Viktor Hapiak (viktor.hapiak@apostera.com)
// @project      MXR
// @sw_interface UIModelsNative/IUIModelNative@1.0
// @ip_type      BGIP
// @brief
//

#ifndef HEADER_GUARD_DDUHNHKLZWEQIXDY
#define HEADER_GUARD_DDUHNHKLZWEQIXDY

#include <memory>

#include "UIModels/IUIModel.hpp"
#include "UIModels/UIModelsTypes.hpp"

namespace MXR {
namespace UIModelsNative {

class IUIModelNative : virtual public UIModels::IUIModel
{
public: // method
    virtual void setInfo(UIModels::ModelId id, std::string const& name) = 0;

    virtual void fireChanges(UIModels::Timestamp timestamp) = 0;

    virtual void fireEvents(UIModels::Timestamp timestamp) = 0;
    
    virtual void applyChanges(UIModels::Timestamp timestamp) = 0;

    virtual void applyEvents(UIModels::Timestamp timestamp) = 0;
};

using UIModelNativePtr = std::shared_ptr<IUIModelNative>;
using UIModelNativeUPtr = std::unique_ptr<IUIModelNative>;

} // namespace UIModels
} // namespace MXR

#endif // HEADER_GUARD_DDUHNHKLZWEQIXDY
