/////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2021 by Apostera GmbH                                     //
/////////////////////////////////////////////////////////////////////////////
//
// @file
// @author          Viktor Hapiak (viktor.hapiak@apostera.com)
// @project         MXR
// @domain          Utility
// @component       UIModelsNative
// @sw_unit         TCommand
// @sw_unit_version 1.0
// @ip_type         BGIP
// @brief
//

#ifndef HEADER_GUARD_BEAEVDBYTSIFHKUF
#define HEADER_GUARD_BEAEVDBYTSIFHKUF

#include <utility>

#include "UIModelsNative/TCommand.hpp"

namespace MXR {
namespace UIModelsNative {

namespace Private {

template <typename Listener, typename Tuple, std::size_t... I>
void call(Listener listener, Tuple const& args, std::index_sequence<I...>)
{
    listener(std::get<I>(args)...);
}

} // namespace Private

template <typename... Args>
void TCommand<Args...>::call(Listener listener) const
{
    for (auto const& args : m_current_calls)
    {
        Private::call(listener, args, std::make_index_sequence<sizeof...(Args)>{});
    }
}

template <typename... Args>
void TCommand<Args...>::deferedCall(Args const&... args)
{
    m_upcoming_calls.emplace_back(args...);
}

template <typename... Args>
void TCommand<Args...>::fireUpdates(UIModels::Timestamp timestamp)
{
    m_updates.emplace(timestamp, std::move(m_upcoming_calls));
    m_upcoming_calls = {};
}

template <typename... Args>
void TCommand<Args...>::applyUpdates(UIModels::Timestamp timestamp)
{
    m_current_calls.clear();
    while (!m_updates.empty() && m_updates.begin()->first <= timestamp)
    {
        auto first = m_updates.begin();
        m_current_calls.insert(m_current_calls.begin(), first->second.begin(), first->second.end());
        m_updates.erase(first);
    }
}

} // namespace UIModelsNative
} // namespace MXR

#endif // HEADER_GUARD_BEAEVDBYTSIFHKUF
