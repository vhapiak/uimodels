/////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2021 by Apostera GmbH                                     //
/////////////////////////////////////////////////////////////////////////////
//
// @file
// @author          Viktor Hapiak (viktor.hapiak@apostera.com)
// @project         MXR
// @domain          Utility
// @component       UIModelsNative
// @sw_unit         TEvent
// @sw_unit_version 1.0
// @ip_type         BGIP
// @brief
//

#ifndef HEADER_GUARD_ZZVILDPDVFZMEXYH
#define HEADER_GUARD_ZZVILDPDVFZMEXYH

#include "UIModelsNative/TEvent.hpp"

namespace MXR {
namespace UIModelsNative {

template <typename T>
std::vector<T> const& TEvent<T>::getTriggers() const
{
    return m_current_triggers;
}

template <typename T>
void TEvent<T>::trigger(T const& value)
{
    m_upcoming_triggers.emplace_back(value);
}

template <typename T>
void TEvent<T>::fireUpdates(UIModels::Timestamp timestamp)
{
    m_updates.emplace(timestamp, std::move(m_upcoming_triggers));
    m_upcoming_triggers = {};
}

template <typename T>
void TEvent<T>::applyUpdates(UIModels::Timestamp timestamp)
{
    m_current_triggers.clear();
    while (!m_updates.empty() && m_updates.begin()->first <= timestamp)
    {
        auto first = m_updates.begin();
        m_current_triggers.insert(
            m_current_triggers.begin(), first->second.begin(), first->second.end());
        m_updates.erase(first);
    }
}

} // namespace UIModelsNative
} // namespace MXR

#endif // HEADER_GUARD_ZZVILDPDVFZMEXYH
