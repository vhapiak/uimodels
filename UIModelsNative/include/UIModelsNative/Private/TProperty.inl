/////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2021 by Apostera GmbH                                     //
/////////////////////////////////////////////////////////////////////////////
//
// @file
// @author          Viktor Hapiak (viktor.hapiak@apostera.com)
// @project         MXR
// @domain          Utility
// @component       UIModelsNative
// @sw_unit         TProperty
// @sw_unit_version 1.0
// @ip_type         BGIP
// @brief
//

#ifndef HEADER_GUARD_YSZNJXHRYHCCAQSF
#define HEADER_GUARD_YSZNJXHRYHCCAQSF

#include "UIModelsNative/TProperty.hpp"

namespace MXR {
namespace UIModelsNative {

template <typename T>
TProperty<T>::TProperty(T default_value, bool cacheable)
    : m_cacheable{cacheable}
    , m_value{std::move(default_value)}
    , m_updated{false}
{
}

template <typename T>
T const& TProperty<T>::getValue() const
{
    return m_value;
}

template <typename T>
bool TProperty<T>::isUpdated() const
{
    return m_updated;
}

template <typename T>
void TProperty<T>::setValue(T const& value)
{
    /// @todo implement caching
    m_current_update = value;
}

template <typename T>
void TProperty<T>::fireUpdates(UIModels::Timestamp timestamp)
{
    if (m_current_update.is_initialized())
    {
        m_updates.emplace(timestamp, std::move(*m_current_update));
        m_current_update = {};
    }
}

template <typename T>
void TProperty<T>::applyUpdates(UIModels::Timestamp timestamp)
{
    m_updated = false;
    while (!m_updates.empty() && m_updates.begin()->first <= timestamp)
    {
        auto first = m_updates.begin();

        m_updated = true;
        m_value = std::move(first->second);

        m_updates.erase(first);
    }
}

} // namespace UIModelsNative
} // namespace MXR

#endif // HEADER_GUARD_YSZNJXHRYHCCAQSF
