/////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2021 by Apostera GmbH                                     //
/////////////////////////////////////////////////////////////////////////////
//
// @file
// @author       Viktor Hapiak (viktor.hapiak@apostera.com)
// @project      MXR
// @sw_interface UIModelsNative/TProperty@1.0
// @ip_type      BGIP
// @brief
//

#ifndef HEADER_GUARD_VMODOECSLDJOYVOT
#define HEADER_GUARD_VMODOECSLDJOYVOT

#include "UIModels/Service/TProperty.hpp"
#include "UIModels/UIModelsTypes.hpp"

// IMPLEMENTATION_DETAILS_BEGINS
#include <map> 
#include <boost/optional.hpp>
// IMPLEMENTATION_DETAILS_ENDS

namespace MXR {
namespace UIModelsNative {

template <typename T>
class TProperty : public MXR::UIModels::Service::TProperty<T>
{
public: // method
    explicit TProperty(T default_value, bool cacheable);

    T const& getValue() const override;

    bool isUpdated() const override;

    void setValue(T const& value);

    void fireUpdates(UIModels::Timestamp timestamp);

    void applyUpdates(UIModels::Timestamp timestamp);

    // IMPLEMENTATION_DETAILS_BEGINS
private: // fields
    const bool m_cacheable;
    
    T m_value;
    bool m_updated;

    boost::optional<T> m_current_update;
    std::map<UIModels::Timestamp, T> m_updates;
    // IMPLEMENTATION_DETAILS_ENDS
};

} // namespace UIModels
} // namespace MXR

#include "UIModelsNative/Private/TProperty.inl"

#endif // HEADER_GUARD_VMODOECSLDJOYVOT
