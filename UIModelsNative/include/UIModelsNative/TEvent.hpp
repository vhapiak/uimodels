/////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2021 by Apostera GmbH                                     //
/////////////////////////////////////////////////////////////////////////////
//
// @file
// @author       Viktor Hapiak (viktor.hapiak@apostera.com)
// @project      MXR
// @sw_interface UIModelsNative/TEvent@1.0
// @ip_type      BGIP
// @brief
//

#ifndef HEADER_GUARD_CIXCQHPSBCZBHGCP
#define HEADER_GUARD_CIXCQHPSBCZBHGCP

#include "UIModels/Client/TEvent.hpp"
#include "UIModels/UIModelsTypes.hpp"

// IMPLEMENTATION_DETAILS_BEGINS
#include <map>
#include <vector>
// IMPLEMENTATION_DETAILS_ENDS

namespace MXR {
namespace UIModelsNative {

template <typename T>
class TEvent : public MXR::UIModels::Client::TEvent<T>
{
public: // method
    std::vector<T> const& getTriggers() const override;

    void trigger(T const& value);

    void fireUpdates(UIModels::Timestamp timestamp);

    void applyUpdates(UIModels::Timestamp timestamp);

    // IMPLEMENTATION_DETAILS_BEGINS
private: // types
    using List = std::vector<T>;
    using Map = std::map<UIModels::Timestamp, List>;

private: // fields
    List m_current_triggers;

    List m_upcoming_triggers;
    Map m_updates;
    // IMPLEMENTATION_DETAILS_ENDS
};

} // namespace UIModelsNative
} // namespace MXR

#include "UIModelsNative/Private/TEvent.inl"

#endif // HEADER_GUARD_CIXCQHPSBCZBHGCP
