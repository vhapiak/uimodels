add_library(MXR_UIModelsNative_Iface INTERFACE)

target_include_directories(
    MXR_UIModelsNative_Iface
    INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}/include/
)

target_link_libraries(
    MXR_UIModelsNative_Iface
    INTERFACE MXR_UIModels_Iface

    INTERFACE Boost::boost
)

add_subdirectory(src)