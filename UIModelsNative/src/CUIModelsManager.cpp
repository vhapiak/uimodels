/////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2021 by Apostera GmbH                                     //
/////////////////////////////////////////////////////////////////////////////
//
// @file
// @author          Viktor Hapiak (viktor.hapiak@apostera.com)
// @project         MXR
// @domain          Utility
// @component       UIModelsNative
// @sw_unit         CUIModelsManager
// @sw_unit_version 1.0
// @ip_type         BGIP
// @brief
//

#include <memory>

#include "CUIModelsManager.hpp"

namespace MXR {
namespace UIModelsNative {

CUIModelsManager::CUIModelsManager() noexcept
    : m_next_id{0u}
{
}

void CUIModelsManager::addModel(std::string const& name, UIModels::UIModelPtr const& model)
{
    std::lock_guard<std::mutex> lock{m_mutex};

    /// @todo can be replaced with static cast and custom checks via IUIModel::getInfo()
    auto ptr = std::dynamic_pointer_cast<IUIModelNative>(model);
    if (ptr == nullptr)
    {
        throw std::runtime_error("Trying to add model of another backend");
    }

    if (m_unique_names.count(name))
    {
        throw std::runtime_error("Model with name " + name + " already registred");
    }

    m_unique_names.insert(name);
    ptr->setInfo(m_next_id++, name);
    m_models.push_back(ptr);
}

std::vector<UIModels::UIModelPtr> CUIModelsManager::getModels() const
{
    std::lock_guard<std::mutex> lock{m_mutex};

    std::vector<UIModels::UIModelPtr> result;
    result.reserve(m_models.size());
    for (auto& model : m_models)
    {
        result.push_back(model);
    }
    return result;
}

void CUIModelsManager::fireChanges(UIModels::Timestamp timestamp)
{
    std::lock_guard<std::mutex> lock{m_mutex};

    for (auto& model : m_models)
    {
        model->fireChanges(timestamp);
    }
}

void CUIModelsManager::fireEvents(UIModels::Timestamp timestamp)
{
    std::lock_guard<std::mutex> lock{m_mutex};

    for (auto& model : m_models)
    {
        model->fireEvents(timestamp);
    }
}

void CUIModelsManager::applyChanges(UIModels::Timestamp timestamp)
{
    std::lock_guard<std::mutex> lock{m_mutex};

    for (auto& model : m_models)
    {
        model->applyChanges(timestamp);
    }
}

void CUIModelsManager::applyEvents(UIModels::Timestamp timestamp)
{
    std::lock_guard<std::mutex> lock{m_mutex};

    for (auto& model : m_models)
    {
        model->applyEvents(timestamp);
    }
}

} // namespace UIModelsNative
} // namespace MXR
