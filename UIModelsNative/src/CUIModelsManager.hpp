/////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2021 by Apostera GmbH                                     //
/////////////////////////////////////////////////////////////////////////////
//
// @file
// @author          Viktor Hapiak (viktor.hapiak@apostera.com)
// @project         MXR
// @domain          Utility
// @component       UIModelsNative
// @sw_unit         CUIModelsManager
// @sw_unit_version 1.0
// @ip_type         BGIP
// @brief
//

#ifndef HEADER_GUARD_EVNMDFUFLWNBSDGY
#define HEADER_GUARD_EVNMDFUFLWNBSDGY

#include <memory>
#include <unordered_set>
#include <vector>
#include <mutex>

#include "UIModels/IUIModelsManagerClient.hpp"
#include "UIModels/IUIModelsManagerService.hpp"

#include "UIModelsNative/IUIModelNative.hpp"

namespace MXR {
namespace UIModelsNative {

class CUIModelsManager
    : public UIModels::IUIModelsManagerClient
    , public UIModels::IUIModelsManagerService
{
public: // method
    explicit CUIModelsManager() noexcept;

    void addModel(std::string const& name, UIModels::UIModelPtr const& model) override;

    std::vector<UIModels::UIModelPtr> getModels() const override;

    void fireChanges(UIModels::Timestamp timestamp) override;

    void fireEvents(UIModels::Timestamp timestamp) override;

    void applyChanges(UIModels::Timestamp timestamp) override;

    void applyEvents(UIModels::Timestamp timestamp) override;

private: // fields
    mutable std::mutex m_mutex;
    
    UIModels::ModelId m_next_id;
    std::unordered_set<std::string> m_unique_names;
    std::vector<UIModelNativePtr> m_models;
};

using UIModelsManagerPtr = std::shared_ptr<CUIModelsManager>;

} // namespace UIModels
} // namespace MXR

#endif // HEADER_GUARD_EVNMDFUFLWNBSDGY
