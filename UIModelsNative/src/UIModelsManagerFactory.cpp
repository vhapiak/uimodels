/////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2021 by Apostera GmbH                                     //
/////////////////////////////////////////////////////////////////////////////
//
// @file
// @author          Viktor Hapiak (viktor.hapiak@apostera.com)
// @project         MXR
// @domain          Utility
// @component       UIModelsNative
// @sw_unit         UIModelsManagerFactory
// @sw_unit_version 1.0
// @ip_type         BGIP
// @brief
//

#include <unordered_map>

#include "CUIModelsManager.hpp"

namespace MXR {

namespace UIModelsNative {

UIModelsManagerPtr getInstance(std::string const& name)
{
    static std::unordered_map<std::string, std::weak_ptr<CUIModelsManager>> instances;

    auto it = instances.find(name);
    if (it == instances.end() || it->second.lock() == nullptr)
    {
        auto ptr = std::make_shared<CUIModelsManager>();
        instances.insert({name, ptr});
        return ptr;
    }

    return it->second.lock();
}

} // namespace UIModelsNative

namespace UIModels {

namespace UIModelsManagerClientFactory {

UIModelsManagerClientPtr createInstace(std::string const& name)
{
    return ::MXR::UIModelsNative::getInstance(name);
}
} // namespace UIModelsManagerClientFactory

namespace UIModelsManagerServiceFactory {

UIModelsManagerServicePtr createInstace(std::string const& name)
{
    return ::MXR::UIModelsNative::getInstance(name);
}
} // namespace UIModelsManagerServiceFactory

} // namespace UIModels
} // namespace MXR