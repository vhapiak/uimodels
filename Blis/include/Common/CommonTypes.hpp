
#pragma once

#include <glm/vec2.hpp>
#include <glm/gtc/quaternion.hpp>




namespace MXR {
namespace Common {

        struct Camera
        {
            ::glm::vec2 position;
            ::glm::quat rotation;
            ::glm::ivec2 size;
            
        };
    

}
}
