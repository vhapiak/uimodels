
#pragma once

#include <cstdint>


#include "Common/CommonTypes.hpp"

namespace ARBox {
namespace Models {
namespace Blis {

        enum class EState
        {
            Inactive,
            Active,
            Warning,
            
        };
    
        struct BlisState
        {
            ::MXR::Common::Camera camera;
            EState state;
            int32_t left;
            uint32_t right;
            double confidence;
            
        };
    
        struct Aggregate
        {
            BlisState left;
            BlisState right;
            
        };
    

}
}
}
