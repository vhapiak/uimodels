#pragma once

#include "UIModels/IUIModel.hpp"

#include "Blis/Client/IBlisClientModel.hpp"

namespace ARBox {
namespace Models {
namespace Blis {
namespace Client {
  
BlisClientModelPtr createInstance(::MXR::UIModels::UIModelPtr const& ptr);

}
}
}
}
