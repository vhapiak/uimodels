#pragma once

#include <memory>

#include <cstdint>
#include <glm/vec2.hpp>


#include "UIModels/IUIModel.hpp"
#include "UIModels/Client/TEvent.hpp"

#include "Blis/BlisTypes.hpp"

namespace ARBox {
namespace Models {
namespace Blis {
namespace Client {

class IBlisClientModel
    : virtual public ::MXR::UIModels::IUIModel
{
public: // methods
    virtual ~IBlisClientModel() = default;

    virtual void setEnabled(bool const& value) noexcept = 0; 
    virtual void setState(BlisState const& value) noexcept = 0; 
    

    virtual void callActivate(::glm::vec2 const& position) noexcept = 0; 
    virtual void callDeactivate() noexcept = 0; 
    

    virtual ::MXR::UIModels::Client::TEvent<bool> const& 
        getActivationFinishedEvent() const noexcept = 0; 
    
};

using BlisClientModelUPtr = std::unique_ptr<IBlisClientModel>;
using BlisClientModelPtr = std::shared_ptr<IBlisClientModel>;

}
}
}
}
