#pragma once

#include <memory>

#include <cstdint>
#include <glm/vec2.hpp>


#include "UIModels/IUIModel.hpp"
#include "UIModels/Service/TProperty.hpp"
#include "UIModels/Service/TCommand.hpp"

#include "Blis/BlisTypes.hpp"

namespace ARBox {
namespace Models {
namespace Blis {
namespace Service {

class IBlisServiceModel
    : virtual public ::MXR::UIModels::IUIModel
{
public: // types
    using ActivateCommand = ::MXR::UIModels::Service::TCommand<::glm::vec2>; 
    using DeactivateCommand = ::MXR::UIModels::Service::TCommand<>; 
    

public: // methods
    virtual ~IBlisServiceModel() = default;

    virtual ::MXR::UIModels::Service::TProperty<bool> const& 
        getEnabledProperty() const noexcept = 0; 
    virtual ::MXR::UIModels::Service::TProperty<BlisState> const& 
        getStateProperty() const noexcept = 0; 
    

    virtual ActivateCommand const& getActivateCommand() noexcept = 0; 
    virtual DeactivateCommand const& getDeactivateCommand() noexcept = 0; 
    

    virtual void triggerActivationFinished(bool const& value) noexcept = 0; 
    
};

using BlisServiceModelUPtr = std::unique_ptr<IBlisServiceModel>;
using BlisServiceModelPtr = std::shared_ptr<IBlisServiceModel>;

}
}
}
}
