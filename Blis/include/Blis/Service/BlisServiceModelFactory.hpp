#pragma once

#include "Blis/Service/IBlisServiceModel.hpp"

namespace ARBox {
namespace Models {
namespace Blis {
namespace Service {
  
BlisServiceModelUPtr createInstance();

}
}
}
}
