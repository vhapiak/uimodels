#include "CBlisModel.hpp"

namespace ARBox {
namespace Models {
namespace Blis {
CBlisModel::CBlisModel() noexcept 
    : ::MXR::UIModels::IUIModel{}
    , m_info{0u, "", "::ARBox::Models::Blis::Blis"}

, mEnabledProperty{ false, true }
, mStateProperty{ {}, false }

{
}

::MXR::UIModels::UIModelInfo CBlisModel::getInfo() const 
{
    return m_info;
}

void CBlisModel::setInfo(::MXR::UIModels::ModelId id, std::string const& name) 
{
    m_info.id = id;
    m_info.name = name;
}

void CBlisModel::fireChanges(::MXR::UIModels::Timestamp timestamp) 
{
    mEnabledProperty.fireUpdates(timestamp);
    mStateProperty.fireUpdates(timestamp);

mActivateCommand.fireUpdates(timestamp);mDeactivateCommand.fireUpdates(timestamp);
}

void CBlisModel::fireEvents(::MXR::UIModels::Timestamp timestamp) 
{
mActivationFinishedEvent.fireUpdates(timestamp);
}
    
void CBlisModel::applyChanges(::MXR::UIModels::Timestamp timestamp) 
{
    mEnabledProperty.applyUpdates(timestamp);
    mStateProperty.applyUpdates(timestamp);

mActivateCommand.applyUpdates(timestamp);mDeactivateCommand.applyUpdates(timestamp);
}

void CBlisModel::applyEvents(::MXR::UIModels::Timestamp timestamp) 
{
mActivationFinishedEvent.applyUpdates(timestamp);
}

void CBlisModel::setEnabled(bool const& value) noexcept 
{
    mEnabledProperty.setValue(value);
} 

::MXR::UIModels::Service::TProperty<bool> const& 
CBlisModel::getEnabledProperty() const noexcept 
{
    return mEnabledProperty;
}
void CBlisModel::setState(BlisState const& value) noexcept 
{
    mStateProperty.setValue(value);
} 

::MXR::UIModels::Service::TProperty<BlisState> const& 
CBlisModel::getStateProperty() const noexcept 
{
    return mStateProperty;
}



void CBlisModel::callActivate(::glm::vec2 const& position) noexcept
{
    mActivateCommand.deferedCall(position);
}

CBlisModel::ActivateCommand const& CBlisModel::getActivateCommand() noexcept
{
    return mActivateCommand;
}
void CBlisModel::callDeactivate() noexcept
{
    mDeactivateCommand.deferedCall();
}

CBlisModel::DeactivateCommand const& CBlisModel::getDeactivateCommand() noexcept
{
    return mDeactivateCommand;
}



void CBlisModel::triggerActivationFinished(bool const& value) noexcept 
{
    mActivationFinishedEvent.trigger(value);
}

::MXR::UIModels::Client::TEvent<bool> const& 
CBlisModel::getActivationFinishedEvent() const noexcept 
{
    return mActivationFinishedEvent;
}


}
}
}
