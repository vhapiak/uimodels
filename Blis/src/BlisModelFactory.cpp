#include <memory>

#include "Blis/Service/BlisServiceModelFactory.hpp"
#include "Blis/Client/BlisClientModelFactory.hpp"
#include "CBlisModel.hpp"

namespace ARBox {
namespace Models {
namespace Blis {
namespace Service {
  
BlisServiceModelUPtr createInstance()
{
    return std::make_unique<CBlisModel>();
}

}

namespace Client {
  
BlisClientModelPtr createInstance(::MXR::UIModels::UIModelPtr const& model)
{
    auto ptr = std::dynamic_pointer_cast<IBlisClientModel>(model);
    if (ptr == nullptr)
    {
        throw std::runtime_error{"Models type mismatch"};
    }
    return ptr;
}

}


}
}
}
