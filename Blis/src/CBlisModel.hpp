#pragma once

#include "Blis/Service/IBlisServiceModel.hpp"
#include "Blis/Client/IBlisClientModel.hpp"

#include "UIModelsNative/IUIModelNative.hpp"
#include "UIModelsNative/TProperty.hpp"
#include "UIModelsNative/TCommand.hpp"
#include "UIModelsNative/TEvent.hpp"

namespace ARBox {
namespace Models {
namespace Blis {
class CBlisModel
    : public Service::IBlisServiceModel
    , public Client::IBlisClientModel
    , ::MXR::UIModelsNative::IUIModelNative
{
public: // methods
    explicit CBlisModel() noexcept;

    ::MXR::UIModels::UIModelInfo getInfo() const override;

    void setInfo(::MXR::UIModels::ModelId id, std::string const& name) override;

    void fireChanges(::MXR::UIModels::Timestamp timestamp) override;

    void fireEvents(::MXR::UIModels::Timestamp timestamp) override;
    
    void applyChanges(::MXR::UIModels::Timestamp timestamp) override;

    void applyEvents(::MXR::UIModels::Timestamp timestamp) override;

    void setEnabled(bool const& value) noexcept override;     
    
    ::MXR::UIModels::Service::TProperty<bool> const& 
        getEnabledProperty() const noexcept override; 
    void setState(BlisState const& value) noexcept override;     
    
    ::MXR::UIModels::Service::TProperty<BlisState> const& 
        getStateProperty() const noexcept override; 
    

    void callActivate(::glm::vec2 const& position) noexcept override; 

    ActivateCommand const& getActivateCommand() noexcept override; 
    void callDeactivate() noexcept override; 

    DeactivateCommand const& getDeactivateCommand() noexcept override; 
    

    void triggerActivationFinished(bool const& value) noexcept override; 

    ::MXR::UIModels::Client::TEvent<bool> const& 
        getActivationFinishedEvent() const noexcept override; 
    

private: // fields
    ::MXR::UIModels::UIModelInfo m_info;

    ::MXR::UIModelsNative::TProperty<bool> mEnabledProperty;
    ::MXR::UIModelsNative::TProperty<BlisState> mStateProperty;
    

    ::MXR::UIModelsNative::TCommand<::glm::vec2> mActivateCommand;
    ::MXR::UIModelsNative::TCommand<> mDeactivateCommand;
    

    ::MXR::UIModelsNative::TEvent<bool> mActivationFinishedEvent;
    
};

}
}
}
