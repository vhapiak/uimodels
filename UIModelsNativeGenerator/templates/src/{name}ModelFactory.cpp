{%- if context.model -%}

#include <memory>

#include "{{context.name}}/Service/{{context.name}}ServiceModelFactory.hpp"
#include "{{context.name}}/Client/{{context.name}}ClientModelFactory.hpp"
#include "C{{context.name}}Model.hpp"

{% for scope in utils.generatePathFromRoot(context.scope) -%}
namespace {{ scope.name }} {
{% endfor -%}

namespace Service {
  
{{context.name}}ServiceModelUPtr createInstance()
{
    return std::make_unique<C{{context.name}}Model>();
}

}

namespace Client {
  
{{context.name}}ClientModelPtr createInstance(::MXR::UIModels::UIModelPtr const& model)
{
    auto ptr = std::dynamic_pointer_cast<I{{context.name}}ClientModel>(model);
    if (ptr == nullptr)
    {
        throw std::runtime_error{"Models type mismatch"};
    }
    return ptr;
}

}


{% for scope in utils.generatePathFromRoot(context.scope) -%}
}
{% endfor -%}

{%- endif -%}