{%- if context.model -%}

#pragma once

#include "{{context.name}}/Service/I{{context.name}}ServiceModel.hpp"
#include "{{context.name}}/Client/I{{context.name}}ClientModel.hpp"

#include "UIModelsNative/IUIModelNative.hpp"
#include "UIModelsNative/TProperty.hpp"
#include "UIModelsNative/TCommand.hpp"
#include "UIModelsNative/TEvent.hpp"

{% for scope in utils.generatePathFromRoot(context.scope) -%}
namespace {{ scope.name }} {
{% endfor -%}

class C{{context.name}}Model
    : public Service::I{{context.name}}ServiceModel
    , public Client::I{{context.name}}ClientModel
    , ::MXR::UIModelsNative::IUIModelNative
{
public: // methods
    explicit C{{context.name}}Model() noexcept;

    ::MXR::UIModels::UIModelInfo getInfo() const override;

    void setInfo(::MXR::UIModels::ModelId id, std::string const& name) override;

    void fireChanges(::MXR::UIModels::Timestamp timestamp) override;

    void fireEvents(::MXR::UIModels::Timestamp timestamp) override;
    
    void applyChanges(::MXR::UIModels::Timestamp timestamp) override;

    void applyEvents(::MXR::UIModels::Timestamp timestamp) override;

    {% for property in context.model.properties -%}
    void set{{property.name}}({{utils.generateTypename(property.type)}} const& value) noexcept override;     
    
    ::MXR::UIModels::Service::TProperty<{{utils.generateTypename(property.type)}}> const& 
        get{{property.name}}Property() const noexcept override; 
    {% endfor %}

    {% for command in context.model.commands -%}
    void call{{command.name}}({{utils.generateArgumentsList(command)}}) noexcept override; 

    {{command.name}}Command const& get{{command.name}}Command() noexcept override; 
    {% endfor %}

    {% for event in context.model.events -%}
    void trigger{{event.name}}({{utils.generateTypename(event.type)}} const& value) noexcept override; 

    ::MXR::UIModels::Client::TEvent<{{utils.generateTypename(event.type)}}> const& 
        get{{event.name}}Event() const noexcept override; 
    {% endfor %}

private: // fields
    ::MXR::UIModels::UIModelInfo m_info;

    {% for property in context.model.properties -%}
    ::MXR::UIModelsNative::TProperty<{{utils.generateTypename(property.type)}}> m{{property.name}}Property;
    {% endfor %}

    {% for command in context.model.commands -%}
    ::MXR::UIModelsNative::TCommand<{{utils.generateTypesList(command)}}> m{{command.name}}Command;
    {% endfor %}

    {% for event in context.model.events -%}
    ::MXR::UIModelsNative::TEvent<{{utils.generateTypename(event.type)}}> m{{event.name}}Event;
    {% endfor %}
};

{% for scope in utils.generatePathFromRoot(context.scope) -%}
}
{% endfor -%}

{%- endif -%}