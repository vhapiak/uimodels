{%- if context.model -%}

{%- macro toBool(value) -%}
{%- if value -%}
    true
{%- else -%}
    false
{%- endif -%}
{%- endmacro -%}

#include "C{{context.name}}Model.hpp"

{% for scope in utils.generatePathFromRoot(context.scope) -%}
namespace {{ scope.name }} {
{% endfor -%}

C{{context.name}}Model::C{{context.name}}Model() noexcept 
    : ::MXR::UIModels::IUIModel{}
    , m_info{0u, "", "{{utils.generateModelType(context.model)}}"}

{% for property in context.model.properties -%}
    , m{{property.name}}Property{ {{property.defaultValue}}, {{toBool(property.cacheable)}} }
{% endfor %}
{
}

::MXR::UIModels::UIModelInfo C{{context.name}}Model::getInfo() const 
{
    return m_info;
}

void C{{context.name}}Model::setInfo(::MXR::UIModels::ModelId id, std::string const& name) 
{
    m_info.id = id;
    m_info.name = name;
}

void C{{context.name}}Model::fireChanges(::MXR::UIModels::Timestamp timestamp) 
{
{%- for property in context.model.properties %}
    m{{property.name}}Property.fireUpdates(timestamp);
{%- endfor %}

{% for command in context.model.commands -%}
    m{{command.name}}Command.fireUpdates(timestamp);
{%- endfor %}
}

void C{{context.name}}Model::fireEvents(::MXR::UIModels::Timestamp timestamp) 
{
{% for event in context.model.events -%}
    m{{event.name}}Event.fireUpdates(timestamp);
{%- endfor %}
}
    
void C{{context.name}}Model::applyChanges(::MXR::UIModels::Timestamp timestamp) 
{
{%- for property in context.model.properties %}
    m{{property.name}}Property.applyUpdates(timestamp);
{%- endfor %}

{% for command in context.model.commands -%}
    m{{command.name}}Command.applyUpdates(timestamp);
{%- endfor %}
}

void C{{context.name}}Model::applyEvents(::MXR::UIModels::Timestamp timestamp) 
{
{% for event in context.model.events -%}
    m{{event.name}}Event.applyUpdates(timestamp);
{%- endfor %}
}

{% for property in context.model.properties -%}
void C{{context.name}}Model::set{{property.name}}({{utils.generateTypename(property.type)}} const& value) noexcept 
{
    m{{property.name}}Property.setValue(value);
} 

::MXR::UIModels::Service::TProperty<{{utils.generateTypename(property.type)}}> const& 
C{{context.name}}Model::get{{property.name}}Property() const noexcept 
{
    return m{{property.name}}Property;
}
{% endfor %}


{% for command in context.model.commands -%}
void C{{context.name}}Model::call{{command.name}}({{utils.generateArgumentsList(command)}}) noexcept
{
    m{{command.name}}Command.deferedCall({{utils.generateVariablesList(command)}});
}

C{{context.name}}Model::{{command.name}}Command const& C{{context.name}}Model::get{{command.name}}Command() noexcept
{
    return m{{command.name}}Command;
}
{% endfor %}


{% for event in context.model.events -%}
void C{{context.name}}Model::trigger{{event.name}}({{utils.generateTypename(event.type)}} const& value) noexcept 
{
    m{{event.name}}Event.trigger(value);
}

::MXR::UIModels::Client::TEvent<{{utils.generateTypename(event.type)}}> const& 
C{{context.name}}Model::get{{event.name}}Event() const noexcept 
{
    return m{{event.name}}Event;
}
{% endfor %}

{% for scope in utils.generatePathFromRoot(context.scope) -%}
}
{% endfor -%}

{%- endif -%}