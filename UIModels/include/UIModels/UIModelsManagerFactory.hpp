/////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2021 by Apostera GmbH                                     //
/////////////////////////////////////////////////////////////////////////////
//
// @file
// @author       Viktor Hapiak (viktor.hapiak@apostera.com)
// @project      MXR
// @sw_interface UIModels/UIModelsManagerFactory@1.0
// @ip_type      BGIP
// @brief
//

#ifndef HEADER_GUARD_SPLMFTFEUPJIZVEV
#define HEADER_GUARD_SPLMFTFEUPJIZVEV

#include <string>

#include "UIModels/IUIModelsManagerClient.hpp"
#include "UIModels/IUIModelsManagerService.hpp"

namespace MXR {
namespace UIModels {

namespace UIModelsManagerClientFactory {
UIModelsManagerClientPtr createInstace(std::string const& name);
}

namespace UIModelsManagerServiceFactory {
UIModelsManagerServicePtr createInstace(std::string const& name);
}

} // namespace UIModels
} // namespace MXR

#endif // HEADER_GUARD_SPLMFTFEUPJIZVEV
