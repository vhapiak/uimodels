/////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2021 by Apostera GmbH                                     //
/////////////////////////////////////////////////////////////////////////////
//
// @file
// @author       Viktor Hapiak (viktor.hapiak@apostera.com)
// @project      MXR
// @sw_interface UIModels/IUIModel@1.0
// @ip_type      BGIP
// @brief
//

#ifndef HEADER_GUARD_IFHEFBGRZBGMTMEF
#define HEADER_GUARD_IFHEFBGRZBGMTMEF

#include <memory>

#include "UIModels/UIModelsTypes.hpp"

namespace MXR {
namespace UIModels {

class IUIModel
{
public: // method
    virtual ~IUIModel() = default;

    virtual UIModelInfo getInfo() const = 0;
};

using UIModelPtr = std::shared_ptr<IUIModel>;
using UIModelUPtr = std::unique_ptr<IUIModel>;

} // namespace UIModels
} // namespace MXR

#endif // HEADER_GUARD_IFHEFBGRZBGMTMEF
