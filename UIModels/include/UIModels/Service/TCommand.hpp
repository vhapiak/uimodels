/////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2021 by Apostera GmbH                                     //
/////////////////////////////////////////////////////////////////////////////
//
// @file
// @author       Viktor Hapiak (viktor.hapiak@apostera.com)
// @project      MXR
// @sw_interface UIModels/TCommand@1.0
// @ip_type      BGIP
// @brief
//

#ifndef HEADER_GUARD_KCPDUSHZUNYZNPNV
#define HEADER_GUARD_KCPDUSHZUNYZNPNV

#include <functional>

namespace MXR {
namespace UIModels {
namespace Service {

template <typename ... Args>
class TCommand
{
public: // types
    using Listener = std::function<void(Args const&...)>; 

public: // method
    virtual ~TCommand() = default;

    ///
    /// @brief Call listener for each call triggered by client
    ///
    virtual void call(Listener listener) const = 0;
};

}
} // namespace UIModels
} // namespace MXR


#endif // HEADER_GUARD_KCPDUSHZUNYZNPNV
