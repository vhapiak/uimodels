/////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2021 by Apostera GmbH                                     //
/////////////////////////////////////////////////////////////////////////////
//
// @file
// @author       Viktor Hapiak (viktor.hapiak@apostera.com)
// @project      MXR
// @sw_interface UIModels/TProperty@1.0
// @ip_type      BGIP
// @brief
//

#ifndef HEADER_GUARD_OIYEMPJVFVSSCSEY
#define HEADER_GUARD_OIYEMPJVFVSSCSEY

namespace MXR {
namespace UIModels {
namespace Service {

template <typename T>
class TProperty
{
public: // method
    virtual ~TProperty() = default;

    virtual T const& getValue() const = 0;

    virtual bool isUpdated() const = 0;
};

}
} // namespace UIModels
} // namespace MXR

#endif // HEADER_GUARD_OIYEMPJVFVSSCSEY
