/////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2021 by Apostera GmbH                                     //
/////////////////////////////////////////////////////////////////////////////
//
// @file
// @author       Viktor Hapiak (viktor.hapiak@apostera.com)
// @project      MXR
// @sw_interface UIModels/IUIModelsManagerService@1.0
// @ip_type      BGIP
// @brief
//

#ifndef HEADER_GUARD_HXUBBPGIOWCAGILT
#define HEADER_GUARD_HXUBBPGIOWCAGILT

#include <memory>
#include <string>

#include "UIModels/IUIModel.hpp"
#include "UIModels/UIModelsTypes.hpp"

namespace MXR {
namespace UIModels {

class IUIModelsManagerService
{
public: // method
    virtual ~IUIModelsManagerService() = default;

    virtual void addModel(std::string const& name, UIModelPtr const& model) = 0;

    virtual void applyChanges(Timestamp timestamp) = 0;

    virtual void fireEvents(Timestamp timestamp) = 0;
};

using UIModelsManagerServicePtr = std::shared_ptr<IUIModelsManagerService>;
using UIModelsManagerServiceUPtr = std::unique_ptr<IUIModelsManagerService>;

} // namespace UIModels
} // namespace MXR

#endif // HEADER_GUARD_HXUBBPGIOWCAGILT
