/////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2021 by Apostera GmbH                                     //
/////////////////////////////////////////////////////////////////////////////
//
// @file
// @author       Viktor Hapiak (viktor.hapiak@apostera.com)
// @project      MXR
// @sw_interface UIModels/IUIModelsManagerClient@1.0
// @ip_type      BGIP
// @brief
//

#ifndef HEADER_GUARD_HJMIMQUEBCAKXYNS
#define HEADER_GUARD_HJMIMQUEBCAKXYNS

#include <memory>
#include <vector>

#include "UIModels/IUIModel.hpp"
#include "UIModels/UIModelsTypes.hpp"

namespace MXR {
namespace UIModels {

class IUIModelsManagerClient
{
public: // method
    virtual ~IUIModelsManagerClient() = default;

    virtual std::vector<UIModelPtr> getModels() const = 0;

    virtual void fireChanges(Timestamp timestamp) = 0;

    virtual void applyEvents(Timestamp timestamp) = 0;
};

using UIModelsManagerClientPtr = std::shared_ptr<IUIModelsManagerClient>;
using UIModelsManagerClientUPtr = std::unique_ptr<IUIModelsManagerClient>;

} // namespace UIModels
} // namespace MXR

#endif // HEADER_GUARD_HJMIMQUEBCAKXYNS
