/////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2021 by Apostera GmbH                                     //
/////////////////////////////////////////////////////////////////////////////
//
// @file
// @author       Viktor Hapiak (viktor.hapiak@apostera.com)
// @project      MXR
// @sw_interface UIModels/TEvent@1.0
// @ip_type      BGIP
// @brief
//

#ifndef HEADER_GUARD_TLXDIQFZGRHCRNIK
#define HEADER_GUARD_TLXDIQFZGRHCRNIK

#include <vector>

namespace MXR {
namespace UIModels {
namespace Client {

template <typename T>
class TEvent
{
public: // method
    virtual ~TEvent() = default;

    virtual std::vector<T> const& getTriggers() const = 0;
};

} // namespace Client
} // namespace UIModels
} // namespace MXR

#endif // HEADER_GUARD_TLXDIQFZGRHCRNIK
