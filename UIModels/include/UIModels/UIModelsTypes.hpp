/////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2021 by Apostera GmbH                                     //
/////////////////////////////////////////////////////////////////////////////
//
// @file
// @author       Viktor Hapiak (viktor.hapiak@apostera.com)
// @project      MXR
// @sw_interface UIModels/UIModelsManagerTypes@1.0
// @ip_type      BGIP
// @brief
//

#ifndef HEADER_GUARD_UAPOQACJIONLDVJA
#define HEADER_GUARD_UAPOQACJIONLDVJA

#include <chrono>
#include <cstdint>
#include <string>

namespace MXR {
namespace UIModels {

using Timestamp = std::chrono::microseconds;
using ModelId = uint32_t;

struct UIModelInfo
{
    ModelId id;
    std::string name; /// unique model name for configuration puproses
    std::string type; /// model full qualified interface name
};


} // namespace UIModels
} // namespace MXR

#endif // HEADER_GUARD_UAPOQACJIONLDVJA
