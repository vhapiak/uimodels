add_library(MXR_UIModels_Iface INTERFACE)

target_include_directories(
    MXR_UIModels_Iface
    INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}/include/
)