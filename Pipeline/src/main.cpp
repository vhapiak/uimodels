
#include <iostream>

#include "UIModels/UIModelsManagerFactory.hpp"

#include "Blis/Client/BlisClientModelFactory.hpp"
#include "Blis/Service/BlisServiceModelFactory.hpp"

struct HMI
{
    HMI()
    {
        m_counter = 0u;

        m_service = MXR::UIModels::UIModelsManagerServiceFactory::createInstace("test");
        m_model = ARBox::Models::Blis::Service::createInstance();

        m_service->addModel("blis", m_model);

        std::cout << "HMI::HMI() enabled default: " << m_model->getEnabledProperty().getValue() << std::endl;
        std::cout << "HMI::HMI() state default: " << m_model->getStateProperty().getValue().camera.position.x
                  << std::endl;
    }

    void iterate(MXR::UIModels::Timestamp timestamp)
    {
        m_service->applyChanges(timestamp);

        std::cout << "HMI::iterate() enabled updated: " << m_model->getEnabledProperty().isUpdated()
                  << std::endl;
        std::cout << "HMI::iterate() enabled value: " << m_model->getEnabledProperty().getValue() << std::endl;

        std::cout << "HMI::iterate() state updated: " << m_model->getStateProperty().isUpdated() << std::endl;
        std::cout << "HMI::iterate() state value: "
                  << m_model->getStateProperty().getValue().camera.position.x << std::endl;

        m_model->getActivateCommand().call([](glm::vec2 const& vec) {
            std::cout << "HMI::iterate() activate called: " << vec.x << std::endl;
        });

        m_model->getDeactivateCommand().call(
            []() { std::cout << "HMI::iterate() deactivate called" << std::endl; });

        if (m_counter % 2)
        {
            m_model->triggerActivationFinished(true);
        }
        ++m_counter;

        m_service->fireEvents(timestamp);
    }

private:
    int32_t m_counter;

    MXR::UIModels::UIModelsManagerServicePtr m_service;
    ARBox::Models::Blis::Service::BlisServiceModelPtr m_model;
};

struct Ctrl
{
    Ctrl()
    {
        m_counter = 0u;

        m_service = MXR::UIModels::UIModelsManagerClientFactory::createInstace("test");
        auto models = m_service->getModels();

        m_model = ARBox::Models::Blis::Client::createInstance(models.front());
    }

    void iterate(MXR::UIModels::Timestamp timestamp)
    {
        m_service->applyEvents(timestamp);

        for (auto trigger : m_model->getActivationFinishedEvent().getTriggers())
        {
            std::cout << "Ctrl::iterate() event triggered: " << trigger << std::endl;
        }

        m_model->setEnabled(m_counter % 2);
        if (m_counter % 2)
        {
            m_model->setState(ARBox::Models::Blis::BlisState{{{m_counter, m_counter + 1}}});
        }

        if (m_counter % 3 == 0)
        {
            m_model->callActivate(glm::vec2{m_counter, m_counter});
            m_model->callDeactivate();
        }

        if (m_counter % 4 == 0)
        {
            m_model->callDeactivate();
        }

        ++m_counter;

        m_service->fireChanges(timestamp);
    }

private:
    int32_t m_counter;
    MXR::UIModels::UIModelsManagerClientPtr m_service;
    ARBox::Models::Blis::Client::BlisClientModelPtr m_model;
};

int main()
{
    HMI hmi;
    Ctrl ctrl;

    for (MXR::UIModels::Timestamp t{0}; t < MXR::UIModels::Timestamp{1000};
         t += MXR::UIModels::Timestamp{100})
    {
        std::cout << std::endl;
        ctrl.iterate(t);
        hmi.iterate(t);
    }

    return 0;
}